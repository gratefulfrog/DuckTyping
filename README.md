# DuckTyping
Python Duck Typing examples

If it quacks, it's a duck.

If it doesn't quack, then it's not a duck!
